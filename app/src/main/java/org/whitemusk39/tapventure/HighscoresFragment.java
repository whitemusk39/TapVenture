package org.whitemusk39.tapventure;


import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.whitemusk39.tapventure.PlayerContract.PlayerEntry;


/**
 * A simple {@link Fragment} subclass.
 */
public class HighscoresFragment extends Fragment {

    ListView scores_lv;
    RelativeLayout content;
    Animation fadein;

    PlayerDBHelper pdh;
    SQLiteDatabase db;
    ArrayList<String> highscores;

    private String[] allColumns = {
            PlayerEntry._ID,
            PlayerEntry.COLUMN_NAME,
            PlayerEntry.COLUMN_SCORE
    };

    public HighscoresFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_highscores, container, false);

        scores_lv = rootView.findViewById(R.id.lv_scores);
        content = rootView.findViewById(R.id.main_frame);
        fadein = AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.fade_in);

        content.startAnimation(fadein);

        pdh = new PlayerDBHelper(getActivity());
        db = pdh.getReadableDatabase();

        loadData();

        // Inflate the layout for this fragment
        return rootView;
    }

    private void loadData() {
        highscores = new ArrayList<>();

        Cursor cursor = db.query(PlayerEntry.TABLE_NAME,
                allColumns,
                null,
                null,
                null,
                null,
                PlayerEntry.COLUMN_SCORE + " DESC",
                null);

        List<Map<String, String>> data = new ArrayList<Map<String, String>>();

        int x = 0;
        cursor.moveToFirst();
        while(!cursor.isAfterLast()) {
            x++;
            Map<String, String> datum = new HashMap<String, String>(2);
            datum.put("pos", Integer.toString(x));
            datum.put("name", cursor.getString(1));
            datum.put("score", "Level " + cursor.getString(2));
            data.add(datum);
            cursor.moveToNext();
        }

        SimpleAdapter adapter = new SimpleAdapter(getActivity().getApplicationContext(), data,
                R.layout.lv_adapter,
                new String[] {"pos", "name", "score"},
                new int[] {R.id.tv_pos,
                            R.id.tv_name,
                            R.id.tv_score});

//        ArrayAdapter<String> adapter = new ArrayAdapter<>(
//                getActivity().getApplicationContext(),
//                R.layout.lv_adapter,
//                highscores
//        );

        scores_lv.setAdapter(adapter);
        cursor.close();
    }

}
