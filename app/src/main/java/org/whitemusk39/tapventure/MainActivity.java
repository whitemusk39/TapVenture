package org.whitemusk39.tapventure;

import android.media.MediaPlayer;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;

public class MainActivity extends AppCompatActivity {

    MediaPlayer bgm, back;
    Animation fadein, fadeout;
    FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);

        setBgmMain();
        playBgm();
        fadein = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in);
        fadeout = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_out);
        back = MediaPlayer.create(getApplicationContext(), R.raw.sfx_back);

        fragmentManager = getSupportFragmentManager();

        // Show splash screen fragment on start
        fragmentManager.beginTransaction().replace(R.id.content_frame, new SplashScreenFragment(), "SplashScreenFragment").commit();
    }

    public void setBgmMain() {
        bgm = MediaPlayer.create(getApplicationContext(), R.raw.bgm_menu);
        bgm.setLooping(true);
    }

    public void setBgmFail() {
        bgm = MediaPlayer.create(getApplicationContext(), R.raw.bgm_fail);
        bgm.setLooping(false);
    }

    public void setBgmBattle() {
        bgm = MediaPlayer.create(getApplicationContext(), R.raw.bgm_heroic_demise);
        bgm.setLooping(true);
    }

    public void releaseBgm() {
        bgm.release();
    }

    public void playBgm() {
        bgm.start();
    }

    public void stopBgm() {
        bgm.stop();
    }

    public boolean isBgmPlaying() {
        if(bgm.isPlaying() && bgm !=null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void onBackPressed() {
        back.start();
        final GameplayFragment gf = (GameplayFragment) fragmentManager.findFragmentByTag("GameplayFragment");
        final Fragment ff = fragmentManager.findFragmentByTag("GameOverFragment");
        final Fragment hf = fragmentManager.findFragmentByTag("HighscoresFragment");
        final Fragment mf = fragmentManager.findFragmentByTag("SplashScreenFragment");
        final Fragment cf = fragmentManager.findFragmentByTag("CreditsFragment");

        if(gf != null && gf.isVisible()) {
            gf.saveLevel(); Message.message(getApplicationContext(), "Progress saved.");
            // findViewById(R.id.content_frame).setBackgroundResource(gf.getCurrentBg());
            super.onBackPressed();

            final Fragment bf = fragmentManager.findFragmentByTag("MainMenuFragment");
            // BGM
            if(isBgmPlaying()) {
                stopBgm();
                releaseBgm();
            }

            setBgmMain();
            playBgm();

            if(bf != null && bf.isVisible()) {
                final RelativeLayout content = bf.getView().findViewById(R.id.main_frame);
                content.startAnimation(fadein);
                content.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        findViewById(R.id.content_frame).setBackgroundResource(R.drawable.bg);
                    }
                }, 500);
            }
        } else if(hf != null && hf.isVisible()) {
            super.onBackPressed();

            findViewById(R.id.content_frame).setBackgroundResource(R.drawable.bg_battle_a);
            final Fragment bf = fragmentManager.findFragmentByTag("MainMenuFragment");
            if(bf != null && bf.isVisible()) {
                final RelativeLayout content = bf.getView().findViewById(R.id.main_frame);
                content.startAnimation(fadein);
                content.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        findViewById(R.id.content_frame).setBackgroundResource(R.drawable.bg);
                    }
                }, 500);
            }
        } else if(mf != null && mf.isVisible()) {
            fragmentManager.beginTransaction().replace(R.id.content_frame, new CreditsFragment(), "CreditsFragment").commit();
            final RelativeLayout content = mf.getView().findViewById(R.id.main_frame);
            content.postDelayed(new Runnable() {
                @Override
                public void run() {
                    finish();
                    System.exit(0);
                }
            }, 3000);
        } else if(cf != null && cf.isVisible()) {
            // do nothing, dont let them press back when credits is showing so that the game will exit after 3 secs
        } else if(ff == null) {
            super.onBackPressed();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(bgm != null && bgm.isPlaying()) {
            bgm.pause();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(bgm != null && !bgm.isPlaying()) {
            bgm.start();
        }
    }


}
