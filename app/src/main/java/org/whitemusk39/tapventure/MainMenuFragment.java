package org.whitemusk39.tapventure;


import android.media.MediaPlayer;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import pl.droidsonroids.gif.GifImageView;

/**
 * A simple {@link Fragment} subclass.
 */
public class MainMenuFragment extends Fragment {

    GifImageView logo;
    ImageButton play, highscore, exit;
    FragmentManager fragmentManager;

    public MainMenuFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main_menu, container, false);
        RelativeLayout content = rootView.findViewById(R.id.main_frame);
        Animation fadein = AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.fade_in);
        final MediaPlayer sfx_select2 = MediaPlayer.create(getActivity().getApplicationContext(), R.raw.sfx_menu_select2);
        final MediaPlayer sfx_select1 = MediaPlayer.create(getActivity().getApplicationContext(), R.raw.sfx_menu_select1);

        fragmentManager = getFragmentManager();
        play = rootView.findViewById(R.id.btn_play);
        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sfx_select1.start();
                getActivity().findViewById(R.id.content_frame).setBackgroundResource(R.drawable.bg);
                fragmentManager.beginTransaction().replace(R.id.content_frame, new GameplayFragment(), "GameplayFragment").addToBackStack("MainMenuFragment").commit();
                play.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        sfx_select1.release();
                        getActivity().findViewById(R.id.content_frame).setBackgroundColor(getResources().getColor(android.R.color.black));

                    }
                },1000);
            }
        });

        highscore = rootView.findViewById(R.id.btn_highscore);
        highscore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sfx_select2.start();
                fragmentManager.beginTransaction().replace(R.id.content_frame, new HighscoresFragment(), "HighscoresFragment").addToBackStack("MainMenuFragment").commit();
                highscore.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        sfx_select2.release();
                    }
                },500);
            }
        });

        exit = rootView.findViewById(R.id.btn_exit);
        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sfx_select2.start();
                fragmentManager.beginTransaction().replace(R.id.content_frame, new CreditsFragment(), "CreditsFragment").commit();
                exit.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        getActivity().finish();
                        System.exit(0);
                    }
                }, 3000);
            }
        });

        return rootView;
    }

}
