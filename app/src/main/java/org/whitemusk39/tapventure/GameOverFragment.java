package org.whitemusk39.tapventure;


import android.app.AlertDialog;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.app.FragmentManager;
import android.content.DialogInterface;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import org.whitemusk39.tapventure.PlayerContract.PlayerEntry;
import org.whitemusk39.tapventure.PlayerContract.PlayerProgress;

/**
 * A simple {@link Fragment} subclass.
 */
public class GameOverFragment extends Fragment {

    int level;

    Animation fadein;
    RelativeLayout content;
    MediaPlayer death;
    FragmentManager fm;

    PlayerDBHelper pdh;
    ProgressDBHelper ppd;
    SQLiteDatabase db, db2;
    ArrayList<String> highscores;

    private String[] allColumns = {
            PlayerEntry._ID,
            PlayerEntry.COLUMN_NAME,
            PlayerEntry.COLUMN_SCORE
    };

    private String[] allColumns2 = {
            PlayerProgress._ID,
            PlayerProgress.COLUMN_LEVEL,
            PlayerProgress.COLUMN_BG
    };

    public GameOverFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_game_over, container, false);
        fm = getFragmentManager();

        if(((MainActivity)getActivity()).isBgmPlaying()) {
            ((MainActivity)getActivity()).stopBgm();
            ((MainActivity)getActivity()).releaseBgm();
        }

        ((MainActivity)getActivity()).setBgmFail();
        ((MainActivity)getActivity()).playBgm();

        level = getArguments().getInt("level");


        pdh = new PlayerDBHelper(getActivity());
        db = pdh.getWritableDatabase();

        ppd = new ProgressDBHelper(getActivity());
        db2 = ppd.getReadableDatabase();

        death = MediaPlayer.create(getActivity().getApplicationContext(), R.raw.sfx_player_die);
        death.start();

        fadein = AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.fade_in_slow);

        content = rootView.findViewById(R.id.fail_frame);
        content.setBackgroundResource(getArguments().getInt("bg"));
        content.startAnimation(fadein);

        content.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                askName();
            }
        });

        // Inflate the layout for this fragment
        return rootView;
    }

    private void resetProgress() {
        String id = "";
        Cursor cursor = db2.query(PlayerProgress.TABLE_NAME,
                allColumns2,
                null,
                null,
                null,
                null,
                null,
                null);

        cursor.moveToFirst();
        while(!cursor.isAfterLast()) {
            id = cursor.getString(0);
            cursor.moveToNext();
        }

        cursor.close();

        ContentValues values = new ContentValues();
        values.put(PlayerProgress.COLUMN_LEVEL, 0);
        values.put(PlayerProgress.COLUMN_BG, R.drawable.bg_battle_a);

        String updateWhereClause = PlayerProgress._ID + " LIKE ?";
        String[] updateWhereArgs = new String[]{ id };

        db2.update(PlayerProgress.TABLE_NAME, values, updateWhereClause, updateWhereArgs);

    }

    public void askName() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("New Highscore!");
        builder.setMessage("You managed to reach level " + level + ". Better luck next time!");

        // Input
        final EditText in = new EditText(getActivity());
        in.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(in);

        // Buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                String name = in.getText().toString();

                ContentValues values = new ContentValues();
                values.put(PlayerEntry.COLUMN_NAME, name);
                values.put(PlayerEntry.COLUMN_SCORE, level);

                insertData(db, values);
                resetProgress();

                for(int j = 0; j < fm.getBackStackEntryCount(); ++j) {
                    fm.popBackStack();
                }

                fm.beginTransaction().replace(R.id.content_frame, new SplashScreenFragment(), "SplashScreenFragment").commit();

                if(((MainActivity)getActivity()).isBgmPlaying()) {
                    ((MainActivity)getActivity()).stopBgm();
                    ((MainActivity)getActivity()).releaseBgm();
                }

                ((MainActivity)getActivity()).setBgmMain();
                ((MainActivity)getActivity()).playBgm();

            }
        });

        builder.show();
    }

    public void insertData(SQLiteDatabase db, ContentValues values) {
        long newRowId;
        newRowId = db.insert(PlayerEntry.TABLE_NAME, null, values);

        if(newRowId != -1) {
            Message.message(getActivity(), "Highscore added!");
        }
    }

}
