package org.whitemusk39.tapventure;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by Hakim Zulkufli on 13-Nov-17.
 */

public class Message {

    public static void message(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

}
