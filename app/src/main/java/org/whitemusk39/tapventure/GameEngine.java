package org.whitemusk39.tapventure;

import android.app.Activity;

/**
 * Created by Hakim Zulkufli on 06-Dec-17.
 */

public class GameEngine {

    private int level;
    private int[][] player;
    private int[] enemy;

    private Activity activity;

    public GameEngine(Activity activity, int lvl) {
        this.level = lvl;
        this.player = createPlayer();
        this.enemy = createEnemy();
        this.activity = activity;
    }

    private int[] createEnemy() {
        int[] enemy1 = new int[3];

        enemy1[0] = (int) (((0.047 * Math.pow(level, 3)) + (0.8 * Math.pow(level, 2)) + (2 * level)) * 10); // Health
        enemy1[2] = enemy1[0];
        enemy1[1] = (int) (enemy1[0] * 0.15); // Damage

        return enemy1;
    }

    private int[][] createPlayer() {
        int[][] player1 = new int[6][2];
        player1[0][0] = (int) (((0.04 * Math.pow(level, 3)) + (0.8 * Math.pow(level, 2)) + (3 * level)) * 10); // Health
        player1[0][1] = (int) (player1[0][0] * 0.90); // Mana

        player1[5][0] = player1[0][0]; // Actual Health
        player1[5][1] = player1[0][1]; // Actual Mana

        // Punch
        player1[1][0] = (int) ((0.04 * Math.pow(level, 3)) + (0.8 * Math.pow(level, 2)) + (1.5 * level) * 10); // Dark Souls mode
        // player1[1][0] = (int) (player1[0][0] * 0.10);
        player1[1][1] = 0;

        int baseDmg = player1[1][0];

        // Sword
        player1[2][0] = (int) (baseDmg * 1.2);
        player1[2][1] = (int) (player1[0][1] * 0.20);

        // Fireball
        player1[3][0] = (int) (baseDmg * 1.4);
        player1[3][1] = (int) (player1[0][1] * 0.25);

        // Heal
        player1[4][0] = (int) (player1[0][0] * 0.40);
        player1[4][1] = (int) (player1[0][1] * 0.18);

        return player1;
    }

    public int getLevel() {
        return this.level;
    }

    public void upLevel() {
        this.level++;
        this.player = createPlayer();
        this.enemy = createEnemy();
    }

    public void resetLevel() {
        this.level = 1;
    }

    // Enemy stats

    public int[] getEnemy() {
        return this.enemy;
    }

    public int getEnemyHP() {
        return getEnemy()[0];
    }

    public int getEnemyHPO() {
        return getEnemy()[2];
    }

    public void reduceEnemyHP(int num) {
        this.enemy[0] -= num;
    }

    public void setEnemyHP(int num) {
        this.enemy[0] = num;
    }

    public int getEnemyDmg() {
        return getEnemy()[1];
    }

    // Player stats

    public int[][] getPlayer() {
        return this.player;
    }

    public int getPlayerHPO() {
        return getPlayer()[0][0];
    }

    public int getPlayerHP() {
        return getPlayer()[5][0];
    }

    public void reducePlayerHP(int num) {
        this.player[5][0] -= num;
    }

    public void setPlayerHP(int num) { this.player[5][0] = num; }

    public void setPlayerMP(int num) { this.player[5][1] = num; }

    public int getPlayerMPO() {
        return getPlayer()[0][1];
    }

    public int getPlayerMP() {
        return getPlayer()[5][1];
    }

    public void reducePlayerMP(int num) {
        this.player[5][1] -= num;
    }

    public int getPlayerPunchDmg() {
        return getPlayer()[1][0];
    }

    public int getPlayerPunchMP() {
        return getPlayer()[1][1];
    }

    public int getPlayerSlashDmg() {
        return getPlayer()[2][0];
    }

    public int getPlayerSlashMP() {
        return getPlayer()[2][1];
    }

    public int getPlayerFireballDmg() {
        return getPlayer()[3][0];
    }

    public int getPlayerFireballMP() {
        return getPlayer()[3][1];
    }

    public int getPlayerHeal() {
        return getPlayer()[4][0];
    }

    public int getPlayerHealMP() {
        return getPlayer()[4][1];
    }

    public void updateProgress() {
        if(getEnemyHP() <= 0) {
            setEnemyHP(0);
        }

        if(getPlayerMP() <= 0) {
            setPlayerMP(0);
        }

        if(getPlayerHP() <= 0) {
            setPlayerHP(0);
        }

        if(getPlayerHP() > getPlayerHPO()) {
            setPlayerHP(getPlayerHPO());
        }
    }

    /*
     * Enemy's moves
     */

    public void enemyAtk() {
        reducePlayerHP(getEnemyDmg());
        updateProgress();
    }

    /*
     * Player's moves
     */

    // Basic attack (Punch)
    public boolean playerPunch() {

        if(getPlayerMP() < getPlayerPunchMP()) {
            return false;
        } else {
            reduceEnemyHP(getPlayerPunchDmg());
            reducePlayerMP(getPlayerPunchMP());
            updateProgress();
            return true;
        }

    }

    // Slash
    public boolean playerSlash() {

        if(getPlayerMP() < getPlayerSlashMP()) {
            return false;
        } else {
            reduceEnemyHP(getPlayerSlashDmg());
            reducePlayerMP(getPlayerSlashMP());
            updateProgress();
            return true;
        }

    }

    public boolean playerFireball() {

        if(getPlayerMP() < getPlayerFireballMP()) {
            return false;
        } else {
            reduceEnemyHP(getPlayerFireballDmg());
            reducePlayerMP(getPlayerFireballMP());
            updateProgress();
            return true;
        }

    }

    public int playerHeal() {

        if(getPlayerMP() < getPlayerHealMP()) {
            return 0;
        } else if(getPlayerHP() == getPlayerHPO()) {
            return 1;
        } else {
            setPlayerHP(getPlayerHP() + getPlayerHeal());
            reducePlayerMP(getPlayerHealMP());
            updateProgress();
            return 2;
        }

    }

}
