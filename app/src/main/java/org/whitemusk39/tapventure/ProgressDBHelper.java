package org.whitemusk39.tapventure;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import org.whitemusk39.tapventure.PlayerContract.PlayerProgress;

/**
 * Created by Data on 6/12/2017.
 */

public class ProgressDBHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    static final String DATABASE_NAME = " ProgressDatabase.db";

    private static final String SQL_CREATE_PROGRESS_TABLE =
            "CREATE TABLE " + PlayerProgress.TABLE_NAME + " (" +
                    PlayerProgress._ID + " INTEGER PRIMARY KEY," +
                    PlayerProgress.COLUMN_LEVEL + " INTEGER NOT NULL, " +
                    PlayerProgress.COLUMN_BG + " INTEGER NOT NULL " +
                    " );";


    public ProgressDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(SQL_CREATE_PROGRESS_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + PlayerProgress.TABLE_NAME);
        onCreate(sqLiteDatabase);
    }
}
