package org.whitemusk39.tapventure;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import org.whitemusk39.tapventure.PlayerContract.PlayerEntry;

/**
 * Created by Data on 6/12/2017.
 */

public class PlayerDBHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    static final String DATABASE_NAME = " PlayerDataBase.db";

    private static final String SQL_CREATE_HIGHSCORES_TABLE =
            "CREATE TABLE " + PlayerEntry.TABLE_NAME + " (" +
                    PlayerEntry._ID + " INTEGER PRIMARY KEY," +
                    PlayerEntry.COLUMN_NAME + " TEXT NOT NULL, "+
                    PlayerEntry.COLUMN_SCORE + " INTEGER NOT NULL "+
                    " );";


    public PlayerDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(SQL_CREATE_HIGHSCORES_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + PlayerEntry.TABLE_NAME);
        onCreate(sqLiteDatabase);
    }
}
