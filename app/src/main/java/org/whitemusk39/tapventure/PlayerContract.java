package org.whitemusk39.tapventure;

import android.provider.BaseColumns;

/**
 * Created by Data on 6/12/2017.
 */

public class PlayerContract {

    public PlayerContract()
    {


    }
    // Inner class that defines the table contents

    public  static final class PlayerEntry implements BaseColumns {

        public static final String TABLE_NAME = "Highscores";
        public static final String COLUMN_NAME = "Player";
        public static final String COLUMN_SCORE = "Score";

    }

    public static final class PlayerProgress implements BaseColumns {

        public static final String TABLE_NAME = "Progress";
        public static final String COLUMN_LEVEL = "Level";
        public static final String COLUMN_BG = "Background";

    }

}
