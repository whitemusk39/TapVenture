package org.whitemusk39.tapventure;


import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.IOException;
import java.util.Random;

import pl.droidsonroids.gif.AnimationListener;
import pl.droidsonroids.gif.GifDrawable;
import pl.droidsonroids.gif.GifImageView;

import org.whitemusk39.tapventure.PlayerContract.PlayerProgress;


/**
 * A simple {@link Fragment} subclass.
 */
public class GameplayFragment extends Fragment {

    TextView level_tv, ehp_tv, playerhp_tv, playermp_tv;
    Button punch_btn, slash_btn, fireball_btn, heal_btn;
    ProgressBar enemyHP, playerHP, playerMP;
    GifImageView enemyGif, explosionGif, sliceGif;
    ImageView bgView;

    RelativeLayout content;
    FragmentManager fm;

    GameEngine ge;
    MediaPlayer sfxPunch, sfxSlash, sfxFireball, sfxHeal, sfxEnemyAtk;
    MediaPlayer enemyDie, playerDie;

    Animation shake, fallDown, fadein, fadeinFast, fadeout, fadeoutFast, atkZoom;
    GifDrawable explode, slice, enemyDraw;

    int enemy_hp, enemy_hpo, player_hp, player_hpo, player_mp, player_mpo, level;
    boolean bgChanged = false;
    int bg_battle = 0, enemy_gif = 0;

    ProgressDBHelper pdh;
    SQLiteDatabase db;

    private String[] allColumns = {
            PlayerProgress._ID,
            PlayerProgress.COLUMN_LEVEL,
            PlayerProgress.COLUMN_BG
    };

    int[] bg_battles = {
            R.drawable.bg_battle_a,
            R.drawable.bg_battle_c,
            R.drawable.bg_battle_d,
            R.drawable.bg_battle_f,
            R.drawable.bg_battle_g,
            R.drawable.bg_battle_h
    };

    int[][] enemy_meta = {
            {R.drawable.e_idle, R.raw.sfx_deathscream_alien1},
            {R.drawable.h_idle, R.raw.sfx_deathscream_alien3},
            {R.drawable.p_idle, R.raw.sfx_deathscream_alien5},
            {R.drawable.q_idle, R.raw.sfx_deathscream_alien4}
    };

    public GameplayFragment() {
        // Required empty public constructor
    }

    private void changeEnemy() throws IOException {
        enemyDraw = new GifDrawable(getResources(), enemy_gif);
        enemyGif.setImageDrawable(enemyDraw);
    }

    private void setRandomEnemy() {
        Random rand = new Random();
        int x = rand.nextInt(4);

        enemy_gif = enemy_meta[x][0];
        try {
            changeEnemy();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void getRandomBg() {
        Random rand = new Random();
        int x = rand.nextInt(6);

        bg_battle = bg_battles[x];
    }

    private void setBgBattle() {
        bgView.setImageResource(bg_battle);
    }

    public int getCurrentBg() {
        return bg_battle;
    }

    private int getSavedLevel() {
        int lvl = 0;

        Cursor cursor = db.query(PlayerProgress.TABLE_NAME,
                allColumns,
                null,
                null,
                null,
                null,
                null,
                null);

        cursor.moveToFirst();
        while(!cursor.isAfterLast()) {
            lvl = Integer.parseInt(cursor.getString(1));
            cursor.moveToNext();
        }

        cursor.close();

        return lvl;
    }

    private int getSavedBg() {
        int bg = 0;

        Cursor cursor = db.query(PlayerProgress.TABLE_NAME,
                allColumns,
                null,
                null,
                null,
                null,
                null,
                null);

        cursor.moveToFirst();
        while(!cursor.isAfterLast()) {
            bg = Integer.parseInt(cursor.getString(2));
            cursor.moveToNext();
        }

        cursor.close();

        return bg;
    }

    public void saveLevel() {
        String id = "";
        Cursor cursor = db.query(PlayerProgress.TABLE_NAME,
                allColumns,
                null,
                null,
                null,
                null,
                null,
                null);

        cursor.moveToFirst();
        while(!cursor.isAfterLast()) {
            id = cursor.getString(0);
            cursor.moveToNext();
        }

        cursor.close();

        ContentValues values = new ContentValues();
        values.put(PlayerProgress.COLUMN_LEVEL, level);
        values.put(PlayerProgress.COLUMN_BG, bg_battle);

        if(getSavedLevel() == 0) {
            db.insert(PlayerProgress.TABLE_NAME, null, values);
        } else {
            String updateWhereClause = PlayerProgress._ID + " LIKE ?";
            String[] updateWhereArgs = new String[]{ id };

            int affectedRow = db.update(PlayerProgress.TABLE_NAME, values, updateWhereClause, updateWhereArgs);
            if(affectedRow > 0) {
                // Message.message(getActivity().getApplicationContext(), "Progress saved.");
            }
        }
    }

    private void initialiseViews(View rootView) throws IOException {
        this.punch_btn = rootView.findViewById(R.id.btn_punch);
        this.slash_btn = rootView.findViewById(R.id.btn_slash);
        this.fireball_btn = rootView.findViewById(R.id.btn_fireball);
        this.heal_btn = rootView.findViewById(R.id.btn_heal);

        // this.info_tv = rootView.findViewById(R.id.tv_info);
        this.level_tv = rootView.findViewById(R.id.tv_level);

        this.enemyHP = rootView.findViewById(R.id.pb_enemy);
        this.playerHP = rootView.findViewById(R.id.pb_playerhp);
        this.playerMP = rootView.findViewById(R.id.pb_playermp);
        this.ehp_tv = rootView.findViewById(R.id.tv_enemyHP);
        this.playerhp_tv = rootView.findViewById(R.id.tv_playerhp);
        this.playermp_tv = rootView.findViewById(R.id.tv_playermp);

        // this.playerGif = rootView.findViewById(R.id.gif_player);
        this.enemyGif = rootView.findViewById(R.id.gif_enemy);
        this.explosionGif = rootView.findViewById(R.id.gif_explosion);
        this.sliceGif = rootView.findViewById(R.id.gif_slice);

        this.content = rootView.findViewById(R.id.game_frame);
        this.bgView = rootView.findViewById(R.id.iv_bg);

        sfxPunch = MediaPlayer.create(getActivity().getApplicationContext(), R.raw.sfx_punch);
        sfxSlash = MediaPlayer.create(getActivity().getApplicationContext(), R.raw.sfx_slash);
        sfxFireball = MediaPlayer.create(getActivity().getApplicationContext(), R.raw.sfx_fireball);
        sfxHeal = MediaPlayer.create(getActivity().getApplicationContext(), R.raw.sfx_heal);
        sfxEnemyAtk = MediaPlayer.create(getActivity().getApplicationContext(), R.raw.sfx_enemyatk);

        enemyDie = MediaPlayer.create(getActivity().getApplicationContext(), R.raw.sfx_enemy_die_1);
        playerDie = MediaPlayer.create(getActivity().getApplicationContext(), R.raw.sfx_player_die);

        shake = AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.atk_shake);
        fallDown = AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.fall_down);
        fadein = AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.fade_in);
        fadeinFast = AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.fade_in_fast);
        fadeout = AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.fade_out);
        fadeoutFast = AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.fade_out_fast);
        atkZoom = AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.atk_zoom);
    }

    private void initialiseEngine(View rootView) throws IOException {
        pdh = new ProgressDBHelper(getActivity());
        db = pdh.getReadableDatabase();

        this.level = getSavedLevel();
        this.bg_battle = getSavedBg();

        if(this.level == 0) {
            this.level = 1;
            this.bg_battle = R.drawable.bg_battle_a;
        }

        setBgBattle();

        this.ge = new GameEngine(getActivity(), this.level);

        explode = new GifDrawable(getResources(), R.drawable.explosion_1);
        explode.stop(); explode.setAlpha(0);
        explosionGif.setImageDrawable(explode);

        slice = new GifDrawable(getResources(), R.drawable.slice_1);
        slice.stop(); slice.setAlpha(0);
        sliceGif.setImageDrawable(slice);

        setRandomEnemy();

        updateProgress();
        updateUI(rootView);

        getActivity().findViewById(R.id.content_frame).setBackgroundColor(getResources().getColor(android.R.color.black));
    }

    private void updateProgress() {
        this.level = this.ge.getLevel();

        this.enemy_hp = this.ge.getEnemyHP();
        this.enemy_hpo = this.ge.getEnemyHPO();

        this.player_hp = this.ge.getPlayerHP();
        this.player_hpo = this.ge.getPlayerHPO();

        this.player_mp = this.ge.getPlayerMP();
        this.player_mpo = this.ge.getPlayerMPO();
    }

    private void updateUI(View rootView) {
        this.enemyHP.setProgress((int)(((double) this.enemy_hp/(double) this.enemy_hpo) * 100));
        this.playerHP.setProgress((int)(((double) this.player_hp/(double) this.player_hpo) * 100));
        this.playerMP.setProgress((int)(((double) this.player_mp/(double) this.player_mpo) * 100));
        this.level_tv.setText("LEVEL " + this.level);
        this.ehp_tv.setText(this.enemy_hp + "/" + this.enemy_hpo);
        this.playerhp_tv.setText(this.player_hp + "/" + this.player_hpo);
        this.playermp_tv.setText(this.player_mp + "/" + this.player_mpo);
    }

    private void disableButtons() {
        punch_btn.setClickable(false);
        slash_btn.setClickable(false);
        fireball_btn.setClickable(false);
        heal_btn.setClickable(false);
    }

    private void enableButtons() {
        punch_btn.setClickable(true);
        slash_btn.setClickable(true);
        fireball_btn.setClickable(true);
        heal_btn.setClickable(true);
    }

    private void enemyAttack() {
        enemyGif.startAnimation(atkZoom);
        this.ge.enemyAtk();
        enemyGif.postDelayed(new Runnable() {
            @Override
            public void run() {
                sfxEnemyAtk.start();
                content.startAnimation(shake);
                content.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        content.clearAnimation();
                        enableButtons();
                    }
                }, 300);
            }
        }, 150);

    }

    private boolean clickPunch() {
        if(ge.playerPunch()) {
            sfxPunch.start();
            enemyGif.startAnimation(shake);
        } else {
            Message.message(getActivity().getApplicationContext(), "Not enough Mana!");
            return false;
        }

        return true;
    }

    private boolean clickSlash() {
        if(ge.playerSlash()) {
            sfxSlash.start();

            slice.setAlpha(255); slice.reset(); enemyGif.startAnimation(shake);
            slice.addAnimationListener(new AnimationListener() {
                @Override
                public void onAnimationCompleted(int loopNumber) {
                    slice.setAlpha(0);
                }
            });

        } else {
            Message.message(getActivity().getApplicationContext(), "Not enough Mana!");
            return false;
        }

        return true;
    }

    private boolean clickFireball() {
        if(ge.playerFireball()) {
            sfxFireball.start();

            explode.setAlpha(255); explode.reset(); enemyGif.startAnimation(shake);
            explode.addAnimationListener(new AnimationListener() {
                @Override
                public void onAnimationCompleted(int loopNumber) {
                    explode.setAlpha(0);
                }
            });

        } else {
            Message.message(getActivity().getApplicationContext(), "Not enough Mana!");
            return false;
        }

        return true;
    }

    private boolean clickHeal() {
        if(ge.playerHeal() == 2) {
            sfxHeal.start();
        } else if(ge.playerHeal() == 1) {
            Message.message(getActivity().getApplicationContext(), "Your Health is full!!");
            return false;
        } else if(ge.playerHeal() == 0) {
            Message.message(getActivity().getApplicationContext(), "Not enough Mana!");
            return false;
        }

        return true;
    }

    private void showGameOver() {
        Bundle bundle = new Bundle();
        bundle.putInt("level", level);
        bundle.putInt("bg", bg_battle);

        Fragment go = new GameOverFragment();
        go.setArguments(bundle);

        getActivity().findViewById(R.id.content_frame).setBackgroundResource(bg_battle);
        fm.beginTransaction().replace(R.id.content_frame, go, "GameOverFragment").commit();
    }

    private void changeBgLvl10() {
        if(this.level % 10 == 0 && !bgChanged) {
            bgChanged = true;
            bgView.startAnimation(fadeoutFast);
            bgView.postDelayed(new Runnable() {
                @Override
                public void run() {
                    bgView.startAnimation(fadeinFast);
                    getRandomBg();
                    setBgBattle();
                }
            }, 100);
        } else if(this.level % 10 != 0) {
            bgChanged = false;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View rootView = inflater.inflate(R.layout.fragment_gameplay, container, false);
        fm = getFragmentManager();

        // BGM
        if(((MainActivity)getActivity()).isBgmPlaying()) {
            ((MainActivity)getActivity()).stopBgm();
            ((MainActivity)getActivity()).releaseBgm();
        }

        ((MainActivity)getActivity()).setBgmBattle();
        ((MainActivity)getActivity()).playBgm();

        try {

            initialiseViews(rootView);
            content.startAnimation(fadein);

            initialiseEngine(rootView);

            // Punch
            punch_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    disableButtons();
                    if(clickPunch()) {
                        updateProgress();
                        updateUI(rootView);

                        punch_btn.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if(enemy_hp > 0) {
                                    enemyAttack();
                                    updateProgress();
                                    updateUI(rootView);

                                    if(player_hp <= 0) {
                                        showGameOver();
                                    }

                                } else {
                                    enemyDie.start();
                                    enemyGif.startAnimation(fallDown);
                                    enemyGif.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            ge.upLevel();
                                            updateProgress();
                                            updateUI(rootView);
                                            enableButtons();
                                            changeBgLvl10();
                                            setRandomEnemy();
                                            saveLevel();
                                        }
                                    }, 800);
                                }
                            }
                        }, 300);
                    } else {
                        enableButtons();
                    }
                }
            });

            // Slash
            slash_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    disableButtons();
                    if(clickSlash()) {
                        updateProgress();
                        updateUI(rootView);

                        slash_btn.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if(enemy_hp > 0) {
                                    enemyAttack();
                                    updateProgress();
                                    updateUI(rootView);

                                    if(player_hp <= 0) {
                                        showGameOver();
                                    }

                                } else {

                                    enemyDie.start();
                                    enemyGif.startAnimation(fallDown);
                                    enemyGif.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            ge.upLevel();
                                            saveLevel();
                                            updateProgress();
                                            updateUI(rootView);
                                            enableButtons();
                                            changeBgLvl10();
                                            setRandomEnemy();
                                            saveLevel();
                                        }
                                    }, 800);
                                }
                            }
                        }, 350);
                    } else {
                        enableButtons();
                    }
                }
            });

            // Fireball
            fireball_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    disableButtons();
                    if(clickFireball()) {
                        updateProgress();
                        updateUI(rootView);

                        fireball_btn.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if(enemy_hp > 0) {
                                    enemyAttack();
                                    updateProgress();
                                    updateUI(rootView);

                                    if(player_hp <= 0) {
                                        showGameOver();
                                    }

                                } else {

                                    enemyDie.start();
                                    enemyGif.startAnimation(fallDown);
                                    enemyGif.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            ge.upLevel();
                                            updateProgress();
                                            updateUI(rootView);
                                            enableButtons();
                                            changeBgLvl10();
                                            setRandomEnemy();
                                            saveLevel();
                                        }
                                    }, 800);
                                }
                            }
                        }, 450);
                    } else {
                        enableButtons();
                    }
                }
            });

            // Heal
            heal_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    disableButtons();
                    if(clickHeal()) {
                        updateProgress();
                        updateUI(rootView);

                        heal_btn.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                enableButtons();
                            }
                        }, 600);
                    } else {
                        enableButtons();
                    }
                }
            });

        } catch (IOException e) {
            e.printStackTrace();
        }

        // Inflate the layout for this fragment
        return rootView;
    }

}
