package org.whitemusk39.tapventure;


import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import pl.droidsonroids.gif.GifImageView;


/**
 * A simple {@link Fragment} subclass.
 */
public class SplashScreenFragment extends Fragment {

    ImageButton play;
    GifImageView logo;
    RelativeLayout content;

    public SplashScreenFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_splash_screen, container, false);

        final MediaPlayer mp = MediaPlayer.create(getActivity().getApplicationContext(), R.raw.sfx_start2);

        // Logo
        logo = rootView.findViewById(R.id.gif_logo);
        Animation zoom_logo = AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.zoom);
        logo.startAnimation(zoom_logo);

        // Start button
        play = rootView.findViewById(R.id.btn_play);

        Animation blink_start = AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.blink_slow);
        play.startAnimation(blink_start);
        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mp.start();

                Animation blink = AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.blink_fast);
                play.startAnimation(blink);

                play.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        FragmentManager fragmentManager = getFragmentManager();
                        fragmentManager.beginTransaction().replace(R.id.content_frame, new MainMenuFragment(), "MainMenuFragment").addToBackStack("SplashScreenFragment").commit();
                        mp.release();
                    }
                }, 1000);

            }
        });

        content = rootView.findViewById(R.id.main_frame);
        content.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mp.start();

                Animation blink = AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.blink_fast);
                play.startAnimation(blink);

                play.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        FragmentManager fragmentManager = getFragmentManager();
                        fragmentManager.beginTransaction().replace(R.id.content_frame, new MainMenuFragment(), "MainMenuFragment").addToBackStack("SplashScreenFragment").commit();
                        mp.release();
                    }
                }, 1000);

            }
        });

        // Inflate the layout for this fragment
        return rootView;
    }

}
